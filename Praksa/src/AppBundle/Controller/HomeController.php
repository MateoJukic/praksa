<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Render;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Repository\PostRepository;
use AppBundle\Repository\InvitationRepository;
use AppBundle\Repository\PollRepository;

class HomeController extends Controller
{
    /**
     * @Route("/home",name="home")
     */
    public function homeAction(PostRepository $postRepo, InvitationRepository $invitationRepo, PollRepository $pollRepo)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) 
        {
            throw $this->createAccessDeniedException();
        }
        
        $posts = $postRepo->findAll();
        $polls = $pollRepo->findAll();
        $invitations = $invitationRepo->findAll();
        return $this->render("home.html.twig",[
            "posts" => $posts,
            "polls" => $polls,
            "invitations" => $invitations,
        ]);
    }
}