<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Post;
use AppBundle\Entity\Status;
use AppBundle\Entity\Comment;
use AppBundle\Repository\PostRepository;
use AppBundle\Repository\CommentRepository;
use AppBundle\Repository\StatusRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Render;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

class PostController extends Controller
{
    /**
     * @Route("/newPost",name="new_post")
     */
    public function postAction(PostRepository $postRepo, StatusRepository $statusRepo, Request $request, EntityManagerInterface $em)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            throw $this->createAccessDeniedException();
        }

        $post = new Post();

        $form = $this->createFormBuilder($post)
            ->add('title', TextType::class)
            ->add('text', TextareaType::class)
            ->add('image', FileType::class,[
                'label' => 'Image upload',
                'required' => false,
                'multiple' => true,
                'mapped' => false,
                /*'constraints' => [
                    new File([
                        'maxSize' => '8M',
                        'mimeTypes' => [
                            'application/jpg',
                            'application/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image.',
                    ])
                ]*/
            ])
            ->add('save', SubmitType::class, ['label' => 'Post'])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $user = $this->getUser();
            $status = $statusRepo->findOneByName("active");
            $post->setAuthor($user);
            $post->setStatus($status);

            $images = $form->get('image')->getData();
            $img_array = [];
            if ($images)
            {
                try 
                {
                    foreach($images as $img)
                    {
                        $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
                       
                        $newFilename = $originalFilename.'-'.uniqid().'.'.$img->guessExtension();

                        array_push($img_array,$newFilename);

                        $img->move(
                            $this->getParameter('images_directory'),
                            $newFilename
                        );
                    }
                }
                catch (FileException $e)
                {
                    // ... handle exception if something happens during file upload
                }
            }
            $post->setImage($img_array);
            $em->persist($post);
            $em->flush($post);

            return $this->redirectToRoute('home');
        }
            
        return $this->render('new_post.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/post/{id}",name="new_post")
     */
    public function postDisplay(
        PostRepository $postRepo,
        CommentRepository $commentRepo,
        StatusRepository $statusRepo,
        Request $request,
        EntityManagerInterface $em,
        $id
    )
    {
        $post = $postRepo->findOneById($id);
        $comment = new Comment();

        $form = $this->createFormBuilder($comment)
            ->add('text', TextareaType::class)
            ->add('image', FileType::class,[
                'label' => 'Image upload',
                'required' => false,
                'multiple' => true,
                'mapped' => false,
                /*'constraints' => [
                    new File([
                        'maxSize' => '8M',
                        'mimeTypes' => [
                            'application/jpg',
                            'application/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image.',
                    ])
                ]*/
            ])
            ->add('save', SubmitType::class, ['label' => 'Comment'])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $user = $this->getUser();
            $status = $statusRepo->findOneByName("active");
            $comment->setAuthor($user);
            $comment->setStatus($status);
            $comment->setPost($post);

            $images = $form->get('image')->getData();
            $img_array = [];

            if ($images)
            {
                try 
                {
                    foreach($images as $img)
                    {
                        $originalFilename = pathinfo($img->getClientOriginalName(), PATHINFO_FILENAME);
                       
                        $newFilename = $originalFilename.'-'.uniqid().'.'.$img->guessExtension();

                        array_push($img_array,$newFilename);

                        $img->move(
                            $this->getParameter('images_directory'),
                            $newFilename
                        );
                    }
                }
                catch (FileException $e)
                {
                    // ... handle exception if something happens during file upload
                }
            }
            $comment->setImage($img_array);
            $em->persist($comment);
            $em->flush($comment);
        }
        $comments = $commentRepo->findByPost($id);
        return $this->render('post.html.twig', [
            'form' => $form->createView(),
            'post' => $post,
            'comments' => $comments,
        ]);

    }
    
}